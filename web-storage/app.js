function submitForm() {
  console.log("In submit form");
}

$(document).ready(function () {
  console.log("In app.js");

  let firstName = localStorage.getItem('firstName');
  let lastName  = localStorage.getItem('lastName');
  $("#savedFirstName").text(firstName);
  $("#savedLastName").text(lastName);

  $("#myForm").submit(function () {
    var formData = new FormData(document.querySelector("#myForm"));
    $("form")
      .serializeArray()
      .forEach((element) => {
        console.log(element);
        localStorage.setItem(element.name, element.value);
      });
  });
});
