Repo de base pour faire les exercices en cours de web avancé.

## RWD

- Appliquer un background blue sur l’élément body pour les écrans dont la largeur est inférieure à 992 pixels

- Appliquer un background vert en mode landscape

- Appliquer une taille de police de 3em à l’élément de classe main-title

- Appliquer une largeur de 100 pixels au bloc 1

- Appliquer une largeur de 50% au bloc 2

- Appliquer une largeur de 80% de la largeur de la fenêtre et une hauteur de 20% de la hauteur de la fenêtre au block 3

## Layout

- Intégrez une grille responsive sur la vue de votre projet à l’aide de Flexbox et/ou Grid Layout avec du contenu fictif. Chaque item doit disposer d’un titre et d’une image.

- La vue s'adapte en fonction de la taille de l'écran.

## PWA

- Ajouter un fichier manifest.json au projet __chat-webtech__

- Ajouter le fichier sw.js et faire un register de ce fichier avec un console.log “SW inscrit”

- Ajouter des handlers pour les événements “install”, “activate” et “fetch” et faire un console.log pour chacun de ces événements

- Mettre en cache les fichiers style.css, index.html, app.js et /img/cat.png

- Afficher dog.png en mode online et cat.png à partir du cache en mode offline

- Mettre à jour le cache si la réponse n’est pas dans le cache.

- Ajouter un bouton sur l’interface pour pouvoir installer l’app. Ce bouton doit être visible uniquement si l’app n’est pas installée

- Ajouter un élément sur l’interface pour signifier le status de la connexion