$(document).ready(function () {
  console.log("In app.js");
  fetch("https://blockchain.info/ticker", {
    method: "GET"
    }).then(response => {
        if (response.ok) {
            response
                .json()
                .then(
                  function(res) {
                    $("#btcPrice").text(res.EUR.last);
                  }).catch(error => {
                    console.error(error);
                });
        } else {
            console.error('server response : ' + response.status);
        }
    }).catch(error => {
        console.error(error);
    });
});
